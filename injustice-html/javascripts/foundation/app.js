;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
  // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
  // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
  // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
  // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

})(jQuery, this);



//auto scroll
$(document).ready(function(){



  $('#navigation').waypoint('sticky');

  $('.image_carousel').find('img').on('mouseover', function(){

    $(this).stop().animate({
      marginTop: '5px'
    }, 200, 'swing');

  });

  $('.image_carousel').find('img').on('mouseout', function(){

    $(this).stop().animate({
      marginTop: '0px'
    }, 200, 'swing');

  });


  $('.image_carousel').find('img').on('click', function(){

    var ele = $(this).attr('data-name');

    var balls = $('#modal_content').find('.' + ele).clone();
    console.log(balls);

    _gaq.push(['_trackEvent', 'character', 'clicked', ele]); 

    $('#myModal2').empty().append(balls).reveal({animation: 'fadeAndPop'});

  });


function checkStop(){

  $('.vote_wrapper').find('iframe').replaceWith('<h2><a href="#_" class="replay">Replay</a></h2>');

}


  $('.vote_wrapper').on('click', 'a.replay', function(e){

    checkStop();

    var sel_vid = $(this).parents('.vote_wrapper').find('.ticked').parent().attr("data-resource");
    $(this).parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_1" width="100%" height="100%" src="http://www.youtube.com/embed/' + sel_vid + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

  });

  //tick functionality
  $('.vote_wrapper').on('click', '.radio_btn', function(e){

    var self = $(this);
    var op = $(this).find('.radio_element');
    var el = op.parent().find('.tick_one');
    var x;


    var container = op.parents('.vote_wrapper');

    if (op.hasClass('ticked') !== true) {

      el.show();
      el.next().show();
      op.addClass('ticked').parents('.character').css('z-index', '998');
      self.parents('.vote_wrapper').find('.blk-overlay').fadeIn(1000);

      if(self.hasClass('left_radio')) {

          self.parents('.vote_wrapper').find('.tv_spot').css('right','9%');

      } else {

          self.parents('.vote_wrapper').find('.tv_spot').css('left','9%');

      }

        self.parents('.vote_wrapper').find('.tv_spot').delay(700).animate({
          top: '10%',
          opacity: 1
        }, 350, 'swing');

        var vid = self.find('span').attr('data-fighter');
        console.log(vid);

       

        switch (vid) {
          case 'lex_luther':

          //checkStop();
            $('#video_1').css('visibility', 'visible');

                
                var selected_video = self.attr("data-resource"); // grab selected video

                console.log(selected_video);

           
           self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_1" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');


          break;

          case 'the_joker':

            checkStop();
           $('#video_2').css('visibility', 'visible').prev().hide();

           var selected_video = self.attr("data-resource");

           
            
            self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_1" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'batman':
          checkStop()
            $('#video_3').css('visibility', 'visible');
            var selected_video = self.attr("data-resource"); 

              // myplayer3.ready(function(){

              //   $('#video_3').hide();

              //   myplayer3.pause();
                
              //   var selected_video = self.attr("data-resource"); // grab selected video

              //     if ( $.browser.msie ) {
              //       $('#video_3').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else if ($.browser.webkit){
              //       $('#video_3').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else {
              //       $('#video_3').find("video:nth-child(1)").attr("src",selected_video + ".webm");
              //    }


              //   $('#video_3').removeClass("vjs-playing").addClass("vjs-paused");

              //   myplayer3.load();

              //   $('#video_3').show();

              //   $('.vjs-big-play-button').hide();

              //   setTimeout (function(){
              //     $('.vjs-big-play-button').show();
              //     myplayer3.play();
              //   }, 2500);

              // });

              //myplayer3.play();

            self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_3" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'superman':
          checkStop()
            $('#video_4').css('visibility', 'visible').prev().hide();

            var selected_video = self.attr("data-resource");

            // myplayer4.ready(function(){

            //     $('#video_4').hide();

            //     myplayer4.pause();
                
            //     var selected_video = self.attr("data-resource"); // grab selected video

            //      if ( $.browser.msie ) {
            //         $('#video_4').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
            //      } else if ($.browser.webkit){
            //         $('#video_4').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
            //      } else {
            //         $('#video_4').find("video:nth-child(1)").attr("src",selected_video + ".webm");
            //      }

            //     $('#video_4').removeClass("vjs-playing").addClass("vjs-paused");

            //     myplayer4.load();

            //     $('#video_4').show();

            //     $('.vjs-big-play-button').hide();

            //     setTimeout (function(){
            //       $('.vjs-big-play-button').show();
            //       myplayer4.play();
            //     }, 2500);

            //   });

              //myplayer4.play();

            self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_4" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');
          break;

          case 'flash':
          checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_5" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/FlashSpecial1.mp4"></video>');
           
            $('#video_6').css('visibility', 'visible').prev().hide();

            var selected_video = self.attr("data-resource");

             // myplayer6.ready(function(){

             //    $('#video_6').hide();

             //    myplayer6.pause();
                
             //    var selected_video = self.attr("data-resource"); // grab selected video

             //    if ( $.browser.msie ) {
             //        $('#video_6').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
             //     } else if ($.browser.webkit){
             //        $('#video_6').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
             //     } else {
             //        $('#video_6').find("video:nth-child(1)").attr("src",selected_video + ".webm");
             //     }

             //    $('#video_6').removeClass("vjs-playing").addClass("vjs-paused");

             //    myplayer6.load();

             //    $('#video_6').show();

             //    $('.vjs-big-play-button').hide();

             //    setTimeout (function(){
             //      $('.vjs-big-play-button').show();
             //      myplayer6.play();
             //    }, 2500);

             //  });

              //myplayer6.play();
              self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_6" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');


          break;

          case 'green_lantern':
          checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/GreenLanternSpecial2.mp4"></video>');
             $('#video_5').css('visibility', 'visible').next().hide();

             var selected_video = self.attr("data-resource");

             // myplayer5.ready(function(){

             //    $('#video_5').hide();

             //    myplayer5.pause();
                
             //    var selected_video = self.attr("data-resource"); // grab selected video

             //    if ( $.browser.msie ) {
             //        $('#video_5').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
             //     } else if ($.browser.webkit){
             //        $('#video_5').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
             //     } else {
             //        $('#video_5').find("video:nth-child(1)").attr("src",selected_video + ".webm");
             //     }

             //    $('#video_5').removeClass("vjs-playing").addClass("vjs-paused");

             //    myplayer5.load();

             //    $('#video_5').show();

             //    $('.vjs-big-play-button').hide();

             //    setTimeout (function(){
             //      $('.vjs-big-play-button').show();
             //      myplayer5.play();
             //    }, 2500);

             //  });

            //myplayer5.play();
            self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_5" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'wonder_woman':
            checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/WonderWomanSpecial2.mp4"></video>');
            $('#video_7').css('visibility', 'visible').next().hide();

            var selected_video = self.attr("data-resource");

            //   myplayer7.ready(function(){

            //     $('#video_7').hide();

            //     myplayer7.pause();
                
            //     var selected_video = self.attr("data-resource"); // grab selected video

            //     if ( $.browser.msie ) {
            //         $('#video_7').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
            //      } else if ($.browser.webkit){
            //         $('#video_7').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
            //      } else {
            //         $('#video_7').find("video:nth-child(1)").attr("src",selected_video + ".webm");
            //      }

            //     $('#video_7').removeClass("vjs-playing").addClass("vjs-paused");

            //     myplayer7.load();

            //     $('#video_7').show();

            //     $('.vjs-big-play-button').hide();

            //     setTimeout (function(){
            //       $('.vjs-big-play-button').show();
            //       myplayer7.play();
            //     }, 2500);

            //   });

              //myplayer7.play();
              self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_7" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'cat_woman':
            checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/CatwomanSpecial2.mp4"></video>');
            $('#video_8').css('visibility', 'visible').prev().hide();
            var selected_video = self.attr("data-resource");

              // myplayer8.ready(function(){

              //   $('#video_8').hide();

              //   myplayer8.pause();
                
              //   var selected_video = self.attr("data-resource"); // grab selected video

              //   if ( $.browser.msie ) {
              //       $('#video_8').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else if ($.browser.webkit){
              //       $('#video_8').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else {
              //       $('#video_8').find("video:nth-child(1)").attr("src",selected_video + ".webm");
              //    }

              //   $('#video_8').removeClass("vjs-playing").addClass("vjs-paused");

              //   myplayer8.load();

              //   $('#video_8').show();

              //   $('.vjs-big-play-button').hide();

              //   setTimeout (function(){
              //     $('.vjs-big-play-button').show();
              //     myplayer8.play();
              //   }, 2500);

              // });

              //myplayer8.play();
              self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_8" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'bane':
            checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/BaneSpecial4.mp4"></video>');
            $('#video_9').css('visibility', 'visible').next().hide();
            var selected_video = self.attr("data-resource");

              // myplayer9.ready(function(){

              //   $('#video_9').hide();

              //   myplayer9.pause();
                
              //   var selected_video = self.attr("data-resource"); // grab selected video

              //   if ( $.browser.msie ) {
              //       $('#video_9').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    }  else if ($.browser.webkit){
              //       $('#video_9').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    }else {
              //       $('#video_9').find("video:nth-child(1)").attr("src",selected_video + ".webm");
              //    }

              //   $('#video_9').removeClass("vjs-playing").addClass("vjs-paused");

              //   myplayer9.load();

              //   $('#video_9').show();

              //   $('.vjs-big-play-button').hide();

              //   setTimeout (function(){
              //     $('.vjs-big-play-button').show();
              //     myplayer9.play();
              //   }, 2500);

              // });

              //myplayer9.play();
              self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_9" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;

          case 'doomsday':
          checkStop()
            //self.parents('.vote_wrapper').find('.tv_spot').html('<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="" preload="auto" autoplay><source type="video/mp4" src="video/DoomsdaySpecial2.mp4"></video>');
            $('#video_10').css('visibility', 'visible').prev().hide();
            var selected_video = self.attr("data-resource");

              // myplayer.ready(function(){

              //   $('#video_10').hide();

              //   myplayer.pause();
                
              //   var selected_video = self.attr("data-resource"); // grab selected video

              //   if ( $.browser.msie ) {
              //       $('#video_10').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else if ($.browser.webkit){
              //       $('#video_10').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
              //    } else {
              //       $('#video_10').find("video:nth-child(1)").attr("src",selected_video + ".webm");
              //    }

              //   $('#video_10').removeClass("vjs-playing").addClass("vjs-paused");

              //   myplayer.load();

              //   $('#video_10').show();

              //   $('.vjs-big-play-button').hide();

              //   setTimeout (function(){
              //     $('.vjs-big-play-button').show();
              //     myplayer.play();
              //   }, 2500);

              // });

              //myplayer.play();

             self.parents('.vote_wrapper').find('.tv_spot').html('<iframe id="#video_9" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

          break;
        }




      //increment all of the totals by 1 when all the check boxes are checked
      if(container.hasClass('bane')) {

          var tckCheck = $('#vote_section').find('.ticked').length;


          if (tckCheck !== 5) {
            //alert('Bro! You only ticked ' + tckCheck + ' boxes! Do em all please!' );
            return false;
          } else {
            $('#vote_section').find('.radio_element').each(function(i){

                if($(this).hasClass('ticked')){
                  var value = $('#vote_count').find('span').eq(i).text();
                  $('#vote_count').find('span').eq(i).text(parseInt(value) + 1);
                }

            });
          }

      }

    } else if(op.hasClass('ticked') == true) {

      // el.hide();
      // el.next().hide();
      // op.removeClass('ticked');

    }


  });


  //bar chart functionality
  $('#vote_section').on('click', '.vote_btn', function(e){

    e.preventDefault();

    var vote_array = [];  //array

    //alert
    var tckCheck = $('#vote_section').find('.ticked').length;

    if (tckCheck !== 5) {
      //alert('Bro! You only ticked ' + tckCheck + ' boxes! Do em all please!' );
      $('#myModal').empty().append('<h4>Hold on!</h4><p class="lead">You only ticked ' + tckCheck + ' boxes!</p><p>Please complete them all to submit your votes.</p><a class="close-reveal-modal">&#215;</a>').reveal({animation: 'fadeAndPop'});
      return false;
    } else if($('#character_stats').hasClass('activated')) {

      //alert('You already submitted one round of votes!');
      $('#myModal').empty().append('<h4>Hold on!</h4><p class="lead">You already submitted one round of votes!</p><p>Please <a href="#_" onClick="window.location.reload()">refresh</a> the page to enter again.</p><a class="close-reveal-modal">&#215;</a>').reveal();

    } else {

    $.scrollTo( '#character_stats', 800, {easing:'swing'});

    //vote activated
    $('#character_stats').addClass('activated');  

    //animate graphs

    var i = 1;
    var e;  

    //number and graph to the left
    $('.figure').each(function(e){

      var el = $(this);

      //calculate percentages
      var num1 = $('.group_' + e).eq(0).text();
      var num2 = $('.group_' + e).eq(1).text();
      var total = (parseInt(num1) + parseInt(num2));

      var num1Percent = Math.round((100/total)*num1);
      console.log(num1);
      console.log(num2);

      //animate bars
      $(this).parent().find('.prog_bar-active.left').animate({
        width: num1Percent + '%'
      }, 1000, 'swing');

      jQuery({someValue: 0}).animate({someValue: num1Percent}, {
        duration: 1000,
        easing:'swing', // can be anything
        step: function() { // called on every step
          // Update the element's text with rounded-up value:
          el.find('span').text(Math.ceil(this.someValue) + '%');  
        }
      });



     });


    //number and graph to the right
    $('.number_holder.right').each(function(e){

        var el = $(this);

        //calculate percentages
        var num1 = $('.group_' + e).eq(0).text();
        var num2 = $('.group_' + e).eq(1).text();
        var total = (parseInt(num1) + parseInt(num2));

        var num2Percent = Math.round((100/total)*num2);


        $(this).parent().find('.prog_bar-active.right').animate({
          width: num2Percent + '%'
        }, 1000, 'swing');

        jQuery({someValue: 0}).animate({someValue: num2Percent}, {
        duration: 1000,
        easing:'swing', // can be anything
          step: function() { // called on every step
            // Update the element's text with rounded-up value:
            el.find('span').text(Math.ceil(this.someValue) + '%');  
          }
        });


          
      });

    } //end of if statement

    });


    $('#carousel').on('click', 'a', function(e){

        e.preventDefault();

        var el = $(this);

        var selected_video = el.attr("href").substring(1);

        // myplayerTrailers.ready(function(){

        //   $('#video_trailers').hide();

        //   myplayerTrailers.pause();
          
        //   var selected_video = el.attr("href").substring(1); // grab selected video

        //   _gaq.push(['_trackEvent', 'video', 'clicked', selected_video]); 

        //  if ( $.browser.msie ) {
        //     $('#video_trailers').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
        //  } else if ($.browser.webkit){
        //     $('#video_trailers').find("video:nth-child(1)").attr("src",selected_video + ".mp4");
        //  } else {
        //     $('#video_trailers').find("video:nth-child(1)").attr("src",selected_video + ".webm");
        //  }

        //   $('#video_trailers').removeClass("vjs-playing").addClass("vjs-paused");

        //   myplayerTrailers.load();

        //   $('#video_trailers').show();

        //   myplayerTrailers.play();

        // });

       $('.selected_video').html('<iframe id="#video_trailers" width="100%" height="100%" src="http://www.youtube.com/embed/' + selected_video + '?autoplay=1" frameborder="0" allowfullscreen></iframe>');

    });

  $('#navigation').find('li').on('click', function(){

    var x = $(this).attr('data-pos');

    if(x == 'vote') {
      $.scrollTo( '#vote_section', 800, {easing:'swing'});
    } else if (x == 'competition') {
      $.scrollTo( '#competition', 800, {easing:'swing'});
    } else if (x == 'character') {
      $.scrollTo( '#character_info', 800, {easing:'swing'});
    } else {
      $.scrollTo( '#about_game', 800, {easing:'swing'});
    }

    console.log('scrolling to >>> ' + x);

  });

  $('.top_btn').find('a').on('click', function(){
    console.log('>>>>>>> go to top');
    $.scrollTo( '#ignHeaderHeader', 800, {easing:'swing'});

  });

});
