<?php

/*checks whether cookie has been set - 
- if cookie not set redirect to form page
- if cookie under age redirect to fail page
- if over age display welcome message
*/
if (isset($_COOKIE["age"]))
{
if ($_COOKIE["age"] == "under") {
  header('Location: form.php');
}
/*else if ($_COOKIE["age"] == "over")
{
echo "welcome to the over 18 page";
}*/
} 
else  header('Location: form.php');

?>

<?php

include('class/HkCompAPI.class.php');

$competition = new HkCompAPI();

    if(isset($_POST['_submit'])){
    
    	$_POST['dob'] = $_POST['_year'].'-'.$_POST['_month'].'-'.$_POST['_day'];
    
	    $validation = array(
	
	    	//@ Format of the array is as follows:
	    	//@ Input Value Name
	    	//@ Error Message
	    	//@ Required, valid_email, over_18, captcha (value must be blank)    	
	    	
			array('fname', 		"Please enter your first name",   			 'required'),
			array('lname', 		"Please enter your last name",    			 'required'),
			array('dob',  		"This promotion is open to users over 18!",	 'over_18'),
			array('email', 		"Please enter a valid email address", 		 'valid_email'),
			array('terms', 		"Please Accept the Terms &amp; Conditions",	 'required')
	
		);  
		
		// validate for errors
		$errors = $competition->validate($validation);


		
		if($_POST["_cap"]!='4753976der'){
			$errors[] = 'The Code you entered was incorrect!';
		 }
		
		
		// validate returns FALSE if no errors are found
		if($errors===FALSE){
			// save the data via the api call with the campaign name
        	$save = $competition->save('ign_injustice');
        	        	
        	if(isset($save['_auth']) && $save['_auth']=='failed'){
	        	echo 'Error Authenticating With the API';     	  	
        	}
             
            // if the data was saved successfully  	
        	if(isset($save['_saved'])==TRUE){
	        
	        //redirect to thankyou page
	        	
	        header('location: thankyou.php');

        	}
          		       
        }
        
        }
                      

?>


<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if lte IE 9]> <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Injustice</title>

	<meta property="og:title" content="Injustice">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="Injustice microsite - Vote in our character pole and enter our competition to win official merch!">
    <meta property="og:url" content="http://uk-microsites.ign.com/injustice">
    <meta property="og:image" content="http://uk-microsites.ign.com/injustice/images/backgrounds/hero_bg.jpg">
    <meta property="fb:admins" content="546507370">


	<!-- Included CSS Files -->
    <link rel="stylesheet" type="text/css" href="stylesheets/demo.css" />
	<link rel="stylesheet" type="text/css" href="stylesheets/elastislide.css" />
	<link rel="stylesheet" type="text/css" href="stylesheets/custom.css" />

	<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
	<link rel="stylesheet" href="stylesheets/app.css">

	<link rel="shortcut icon" href="favicon.ico">
	<link rel=apple-touch-icon href="apple-touch-icon.png">
	<link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
	<link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">


	<script src="javascripts/foundation/modernizr.foundation.js"></script>
	<script src="javascripts/modernizr.custom.17475.js"></script>

	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->

</head>
<body>

	<header id="ignHeaderHeader">
		<div id="ignHeader" class="clear">
			<ul class="social-btns" style="display:block;">
	                <li>
	                    <div class="fb-like" data-href="http://uk-microsites.ign.com/injustice/" data-send="false" data-layout="button_count" data-width="60" data-show-faces="false"></div>
	                </li>

	                <li><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://uk-microsites.ign.com/injustice/" data-text="Come and vote for your favourite DC characters!" data-via="IGNUK">Tweet</a></li>

	                <li>
	                    <div class="g-plusone" data-size="tall" data-annotation="none" data-href="http://uk-microsites.ign.com/injustice/"></div>
	                </li>
	            </ul>
				
				<div id="ignHeader-userBar">
					<div class="container"> <a id="ignHeader-logo" href="http://uk.ign.com/"></a>
					</div>
				</div>

		</div>
	</header>

	<nav id="navigation">
		<ul>
			<li data-pos="vote"><a href="#_">VOTE</a></li>
			<li class="divider"></li>
			<li data-pos="competition"><a href="#_">COMPETITION</a></li>
			<li class="divider"></li>
			<li class="last" data-pos="about"><a href="#_">ABOUT THE GAME</a></li>
			<li class="divider"></li>
			<li class="last" data-pos="character"><a href="#_">CHARACTER INFO</a></li>
		</ul>
	</nav>

	<article>
		<header class="hero_unit">
			<img src="images/backgrounds/hero_title.png" alt="Injustice - Gods Among Us" class="title">

			 <!-- <div id="parallax_mid"> </div> -->
			<!-- <div id="parallax_front"> </div>  -->
			<!-- <div id="parallax_front_2"> </div> -->

			<div class="pre-order_overlay">
				<div class="row">
					<div class="four columns pre-order">
						<a href="http://www.game.co.uk/webapp/wcs/stores/servlet/AjaxCatalogSearch?storeId=10151&catalogId=10201&langId=44&pageSize=20&beginIndex=0&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&predictiveSearchURL=http%3A%2F%2Fwww.game.co.uk%2Fwebapp%2Fwcs%2Fstores%2Fservlet%2FAjaxPredictiveSearchView%3FcatalogId%3D10201%26langId%3D44%26storeId%3D10151&searchTerm=Injustice%3A+Gods+Among+Us&searchBtn.x=12&searchBtn.y=6&searchBtn=Search" target="_blank" onClick="_gaq.push(['_trackEvent', 'button', 'clicked', 'pre-order']);"><img src="images/packshots.png" alt=""></a>
					</div>

					<div class="eight columns">
						<h3><a href="#_">WHO IS THE MOST SUPER OF ALL?</a></h3>
						<p>Injustice: Gods Among Us features a huge cast of DC Comics characters pitted against one another in an original storyline that blurs the traditional lines of good and evil as heroes and villains engage in epic battles on a huge scale. We want to find out which of the iconic characters are most loved, so cast your votes in all of the battles below to have your say - your votes will only register if you vote in every category.</p>
<!-- 						<p>Don’t forget to watch the videos to see some of the amazing locations and moves on offer in Injustice: God Among Us.</p> -->
					</div>
				</div>
			</div>

		</header>

		<!--vote section-->
		<section id="vote_section">
			<div class="lex vote_wrapper">
				<div class="row">
					<div class="four columns inj_first character">
						<div class="character_container">
							<img src="images/characters/lex.png" alt="Lex Luther" class="lex_luther" />
							<h3>LEX LUTHOR</h3>
						</div>
						<div class="radio_btn left_radio" data-resource="video/LexSpecial1">
							<span class="radio_element" data-fighter="lex_luther"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<div class="four columns">
						<div class="versus_bar">
							<div class="border-top"></div>
							<div class="thick_bar">
								<div class="vs_circle">
									<img src="images/vs.png" alt="">
								</div>
							</div>
							<div class="border-btm"></div>
						</div>
					</div>
					<div class="four columns inj_last character">
						<div class="character_container">
							<img src="images/characters/joker.png" alt="The Joker" class="the_joker" />
							<h3>JOKER</h3>
						</div>
						<div class="radio_btn" data-resource="video/JokerSpecial1">
							<span class="radio_element left_radio" data-fighter="the_joker"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>

				</div>
				<div class="blk-overlay"></div>
				<div class="tv_spot">
					<video id="video_1" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  <source type="video/webm" src="">
					</video>
					<video id="video_2" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					 <source type="video/webm" src="">
					</video>
				</div>
			</div>
			<div class="bat vote_wrapper">
				<div class="row">
					<div class="four columns inj_first character">
						<div class="character_container">
							<img src="images/characters/batman.png" alt="The Batman" class="the_batman" />
							<h3>BATMAN</h3>
						</div>
						<div class="radio_btn left_radio" data-resource="video/BatmanSPecial2">
							<span class="radio_element" data-fighter="batman"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<div class="four columns">
						<div class="versus_bar">
							<div class="border-top"></div>
							<div class="thick_bar">
								<div class="vs_circle">
									<img src="images/vs.png" alt="">
								</div>
							</div>
							<div class="border-btm"></div>
						</div>
					</div>
					<div class="four columns inj_last character">
						<div class="character_container">
							<img src="images/characters/superman.png" alt="Superman" class="superman" />
							<h3>SUPERMAN</h3>
						</div>
						<div class="radio_btn" data-resource="video/SupermanSpecial1">
							<span class="radio_element" data-fighter="superman"></span>
						    <div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
				</div>
				<div class="blk-overlay"></div>
				<div class="tv_spot">
					<video id="video_3" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
					<video id="video_4" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  <source type="video/webm" src="">
					</video>
				</div>
			</div>
			<div class="flash vote_wrapper">
				<div class="row">
					<div class="four columns inj_first character">
						<div class="character_container">
							<img src="images/characters/flash.png" alt="The Flash" class="the_flash" />
							<h3>FLASH</h3>
						</div>
						<div class="radio_btn left_radio" data-resource="video/FlashSpecial2">
							<span class="radio_element" data-fighter="flash"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<div class="four columns">
						<div class="versus_bar">
							<div class="border-top"></div>
							<div class="thick_bar">
								<div class="vs_circle">
									<img src="images/vs.png" alt="">
								</div>
							</div>
							<div class="border-btm"></div>
						</div>
					</div>
					<div class="four columns inj_last character">
						<div class="character_container">
							<img src="images/characters/lantern.png" alt="Green Lantern" class="green_lantern" />
							<h3>GREEN LANTERN</h3>
						</div>
						<div class="radio_btn" data-resource="video/GreenLanternSpecial1">
							<span class="radio_element" data-fighter="green_lantern"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
				</div>
				<div class="blk-overlay"></div>
				<div class="tv_spot">
					<video id="video_5" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
					<video id="video_6" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
				</div>
			</div>
			<div class="wonder vote_wrapper">
				<div class="row">
					<div class="four columns inj_first character">
						<div class="character_container">
							<img src="images/characters/wonder.png" alt="Wonder Woman" class="wonder_woman" />
							<h3>WONDER WOMAN</h3>
						</div>
						<div class="radio_btn left_radio" data-resource="video/WonderWomanSpecial2">
							<span class="radio_element" data-fighter="wonder_woman"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<div class="four columns">
						<div class="versus_bar">
							<div class="border-top"></div>
							<div class="thick_bar">
								<div class="vs_circle">
									<img src="images/vs.png" alt="">
								</div>
							</div>
							<div class="border-btm"></div>
						</div>
					</div>
					<div class="four columns inj_last character">
						<div class="character_container">
							<img src="images/characters/cat.png" alt="Cat Women" class="cat_women" />
							<h3>CAT WOMAN</h3>
						</div>
						<div class="radio_btn" data-resource="video/CatwomanSpecial2">
							<span class="radio_element" data-fighter="cat_woman"></span>
							 <div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
				</div>
				<div class="blk-overlay"></div>
				<div class="tv_spot">
					<video id="video_7" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
					<video id="video_8" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
				</div>
			</div>
			<div class="bane vote_wrapper">
				<div class="row">
					<div class="four columns inj_first character">
						<div class="character_container">
							<img src="images/characters/bane.png" alt="Bane" class="bane_char" />
							<h3>BANE</h3>
						</div>
						<div class="radio_btn left_radio" data-resource="video/BaneSpeciail2">
							<span class="radio_element" data-fighter="bane"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<div class="four columns">
						<div class="versus_bar">
							<div class="border-top"></div>
							<div class="thick_bar">
								<div class="vs_circle">
									<img src="images/vs.png" alt="">
								</div>
							</div>
							<div class="border-btm"></div>
						</div>
					</div>
					<div class="four columns inj_last character">
						<div class="character_container">
							<img src="images/characters/doomsday.png" alt="Doomsday" class="doomsday" />
							<h3>DOOMSDAY</h3>
						</div>
						<div class="radio_btn" data-resource="video/DoomsdaySpecial1">
							<span class="radio_element" data-fighter="doomsday"></span>
							<div class="tick_one"></div>
							<div class="tick_two"></div>
						</div>
					</div>
					<!-- <button class="vote_btn gradient">VOTE</button> -->
				</div>
				<div class="blk-overlay"></div>
				<div class="tv_spot">
					<video id="video_9" class="video-js vjs-default-skin" controls width="100%" height="100%" >
					  <source type="video/mp4" src="">
					  	<source type="video/webm" src="">
					</video>
					<video id="video_10" class="video-js vjs-default-skin" controls width="100%" height="100%" >
						<source type="video/mp4" src="">
					  <source type="video/webm" src="">
					</video>
				</div>
				<button class="vote_btn gradient">VOTE</button>
			</div>
		</section>
		<!--end vote section-->

		<!--Character stats-->
		<div class="standard_wrapper">
		<section id="character_stats" class="standard_container">
			<div class="row">
				<div class="container">
					<h3>GLOBAL RESULTS</h3>
					<div class="twelve columns">
						<div class="bg_results">
							<div class="prog_bar-wrapper">
<!-- 									<p>Think Lex Luther would beat the Joker</p> -->
								<div class="number_holder figure">
									<span>0%</span>
								</div>
								<div class="number_holder right">
									<span>0%</span>
								</div>
								<div class="prog_bar">
									<span class="prog_bar-active left"></span>
									<span class="prog_bar-active right"></span>
								</div>
							</div>
						</div>
						<img src="images/characters/lex.png" alt="Lex Luther" class="lex_luther" />
						<img src="images/characters/joker.png" alt="The Joker" class="the_joker" />
					</div>
					<div class="twelve columns">
						<div class="bg_results">
							<div class="prog_bar-wrapper">
<!-- 									<p>Think Lex Luther would beat the Joker</p> -->
								<div class="number_holder figure">
									<span>0%</span>
								</div>
								<div class="number_holder right">
									<span>0%</span>
								</div>
								<div class="prog_bar">
									<span class="prog_bar-active left"></span>
									<span class="prog_bar-active right"></span>
								</div>
							</div>
						</div>
						<img src="images/characters/batman.png" alt="The Batman" class="the_batman" />
						<img src="images/characters/superman.png" alt="Superman" class="superman" />
					</div>
					<div class="twelve columns">
						<div class="bg_results">
							<div class="prog_bar-wrapper">
<!-- 									<p>Think Lex Luther would beat the Joker</p> -->
								<div class="number_holder figure">
									<span>0%</span>
								</div>
								<div class="number_holder right">
									<span>0%</span>
								</div>
								<div class="prog_bar">
									<span class="prog_bar-active left"></span>
									<span class="prog_bar-active right"></span>
								</div>
							</div>
						</div>
						<img src="images/characters/flash.png" alt="The Flash" class="the_flash" />
						<img src="images/characters/lantern.png" alt="Green Lantern" class="green_lantern" />
					</div>
					<div class="twelve columns">
						<div class="bg_results">
							<div class="prog_bar-wrapper">
<!-- 									<p>Think Lex Luther would beat the Joker</p> -->
								<div class="number_holder figure">
									<span>0%</span>
								</div>
								<div class="number_holder right">
									<span>0%</span>
								</div>
								<div class="prog_bar">
									<span class="prog_bar-active left"></span>
									<span class="prog_bar-active right"></span>
								</div>
							</div>
						</div>
						<img src="images/characters/wonder.png" alt="Wonder Woman" class="wonder_woman" />
						<img src="images/characters/cat.png" alt="Cat Women" class="cat_women" />
					</div>
					<div class="twelve columns">
						<div class="bg_results">
							<div class="prog_bar-wrapper">
<!-- 									<p>Think Lex Luther would beat the Joker</p> -->
								<div class="number_holder figure">
									<span>0%</span>
								</div>
								<div class="number_holder right">
									<span>0%</span>
								</div>
								<div class="prog_bar">
									<span class="prog_bar-active left"></span>
									<span class="prog_bar-active right"></span>
								</div>
							</div>
						</div>
						<img src="images/characters/bane.png" alt="Bane" class="bane_char" />
						<img src="images/characters/doomsday.png" alt="Doomsday" class="doomsday" />
					</div>
				</div>
			</div>
		</section>
		<!--end character stats-->

		<!--competition-->
		<section id="competition" class="standard_container">
			<div class="row">
				<div class="container">

				<h3>COMPETITION</h3>
				<p>To celebrate the impending release of Injustice: Gods Among Us on April 19th, we’re giving you the chance to win an incredible home set up that will transform your Batcave or Fortress Of Solitude into the most incredible gaming den ever. The top prize includes a 46” 3D TV, surround sound, a console of your choice (Xbox 360/PS3/Wii U) with an extra controller, a mini-fridge filled with drinks, a DC Blu-ray and book collection, DC Bobblehead character figures, AND a copy of Injustice: Gods Among Us. We’ve also got copies of the game to give to nine runners up too, so submit your details below to enter this amazing competition now!</p>


<?php 
		if(isset($errors) and $errors!=""){
			// if there were errors in form processing show them here
	        echo '<div class="alert alert-error"><h4>Oops!</h4><ul>';
			      foreach($errors as $error){		        
			      echo '<li>'.$error.'</li>';  
		        }	        
	        echo '</ul></div>';
        }
		else if(isset($errors) and $errors==""){
			echo '<script type="text/javascript">'
				   , 'window.location.replace("thankyou.php");'
				   , '</script>';
		}
		
?>

				<form action="" method="post" class="form-horizontal">
				        <div class="control-group first">
				            <label class="control-label">First Name</label>
				            <div class="controls">
					            <input type="text" name="fname" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>"> 
				        	</div>
				        </div>

				        <div class="control-group second">
				            <label class="control-label">Last Name</label>
				            <div class="controls">
					            <input type="text" name="lname" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>"> 
				        	</div>
				        </div>
				     
				        <div class="control-group first">
				            <label class="control-label">Email</label>
				            <div class="controls">
					            <input type="text" name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>"> 
				        	</div>
				        </div>
				        
				        <div class="control-group second">
				            <label class="control-label">DOB</label>
				             <div class="controls">

								
								<select name="_day" class="span1">
								<option value="0">Day</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
								</select>

								<select name="_month" class="span2">
								<option value="0">Month</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
								</select>
								
								<select name="_year" class="span2">
								<option value="0">Year</option>
								<option value="2009">2009</option>
								<option value="2008">2008</option>
								<option value="2007">2007</option>
								<option value="2006">2006</option>
								<option value="2005">2005</option>
								<option value="2004">2004</option>
								<option value="2003">2003</option>
								<option value="2002">2002</option>
								<option value="2001">2001</option>
								<option value="2000">2000</option>
								<option value="1999">1999</option>
								<option value="1998">1998</option>
								<option value="1997">1997</option>
								<option value="1996">1996</option>
								<option value="1995">1995</option>
								<option value="1994">1994</option>
								<option value="1993">1993</option>
								<option value="1992">1992</option>
								<option value="1991">1991</option>
								<option value="1990">1990</option>
								<option value="1989">1989</option>
								<option value="1988">1988</option>
								<option value="1987">1987</option>
								<option value="1986">1986</option>
								<option value="1985">1985</option>
								<option value="1984">1984</option>
								<option value="1983">1983</option>
								<option value="1982">1982</option>
								<option value="1981">1981</option>
								<option value="1980">1980</option>
								<option value="1979">1979</option>
								<option value="1978">1978</option>
								<option value="1977">1977</option>
								<option value="1976">1976</option>
								<option value="1975">1975</option>
								<option value="1974">1974</option>
								<option value="1973">1973</option>
								<option value="1972">1972</option>
								<option value="1971">1971</option>
								<option value="1970">1970</option>
								<option value="1969">1969</option>
								<option value="1968">1968</option>
								<option value="1967">1967</option>
								<option value="1966">1966</option>
								<option value="1965">1965</option>
								<option value="1964">1964</option>
								<option value="1963">1963</option>
								<option value="1962">1962</option>
								<option value="1961">1961</option>
								<option value="1960">1960</option>
								<option value="1959">1959</option>
								<option value="1958">1958</option>
								<option value="1957">1957</option>
								<option value="1956">1956</option>
								<option value="1955">1955</option>
								<option value="1954">1954</option>
								<option value="1953">1953</option>
								<option value="1952">1952</option>
								<option value="1951">1951</option>
								<option value="1950">1950</option>
								<option value="1949">1949</option>
								<option value="1948">1948</option>
								<option value="1947">1947</option>
								<option value="1946">1946</option>
								<option value="1945">1945</option>
								<option value="1944">1944</option>
								<option value="1943">1943</option>
								<option value="1942">1942</option>
								<option value="1941">1941</option>
								<option value="1940">1940</option>
								<option value="1939">1939</option>
								<option value="1938">1938</option>
								<option value="1937">1937</option>
								<option value="1936">1936</option>
								<option value="1935">1935</option>
								<option value="1934">1934</option>
								<option value="1933">1933</option>
								<option value="1932">1932</option>
								<option value="1931">1931</option>
								<option value="1930">1930</option>
								<option value="1929">1929</option>
								<option value="1928">1928</option>
								<option value="1927">1927</option>
								<option value="1926">1926</option>
								<option value="1925">1925</option>
								<option value="1924">1924</option>
								<option value="1923">1923</option>
								<option value="1922">1922</option>
								<option value="1921">1921</option>
								<option value="1920">1920</option>
								<option value="1919">1919</option>
								<option value="1918">1918</option>
								<option value="1917">1917</option>
								<option value="1916">1916</option>
								<option value="1915">1915</option>
								<option value="1914">1914</option>
								<option value="1913">1913</option>
								<option value="1912">1912</option>
								<option value="1911">1911</option>
								<option value="1910">1910</option>
								<option value="1909">1909</option>
								</select>
				             </div>
				        </div>
 
				         <input type="hidden" name="_cap" value="4753976der"/>

				        <div class="clearfix"></div>
				        <div class="control-group full">
				            <div class="controls">
				                <label class="checkbox"><input type="checkbox" name="terms" value="1">Terms &amp; Conditions</label>
				                <span class="help-block">I confirm that I am a UK resident over the age of 18 and have <a href="tandc.php">read and understood the terms and conditions</a></span>
				            </div>
				        </div>
				        
				        <div class="control-group full">
				            <div class="controls">
				                <label class="checkbox"><input type="checkbox" name="subscribe" value="1">Newsletter</label>
				                <span class="help-block">I agree that my email details can be used by IGN Entertainment and Warner Bros. Entertainment UK Limited to keep me informed of exciting new products, promotions and services until I choose otherwise.</span>
				            </div>
				        </div>
				        <div class="clearfix"></div>
				        
				        <div class="control-group full">
				            <div class="controls">

					            <input type="submit" value="ENTER" class="btn-primary btn" name="_submit">
				            </div>
				        </div>
				    </form>  
				</div> <!-- container -->
			</div>
		</section>
		<!--end competition section-->

		<!--about game section-->
		<section id="about_game" class="standard_container">
			<div class="row">
				<div class="container">
					<h3>ABOUT THE GAME</h3>
					<div class="video_asset-cont">

						<div class="selected_video">
							<video id="video_trailers" class="video-js vjs-default-skin" controls width="100%" height="100%" poster="images/line-img.png" onClick="_gaq.push(['_trackEvent', 'video', 'clicked', 'video/Injustice_Arkham_ENG.mp4']);">
							  <source src="video/injustice_line_trailer_eng.mp4" type='video/mp4'>
							  <source src="video/injustice_line_trailer_eng.mp4.webm" type='video/webm'>
							</video>
						</div>

						<!-- Elastislide Carousel -->
					<img src="images/vid_car-bg.png" class="car_bg" alt="">
						<ul id="carousel" class="elastislide-list">

							<li><a href="#video/injustice_line_trailer_eng"><img src="video_jpegs/joker.jpg" alt="image07" /></a></li>
							<li><a href="#video/injustice_versus_eng"><img src="video_jpegs/flash.jpg" alt="image06" /></a></li>
							<li><a href="#video/injustice_lexluthor_eng"><img src="video_jpegs/luther.jpg" alt="image08" /></a></li>
							<li><a href="#video/injustice_gamescom_eng"><img src="video_jpegs/cat.jpg" alt="image08" /></a></li>
							<li><a href="#video/Injustice_Arkham_ENG"><img src="video_jpegs/arkham_pack.jpg" alt="image03" /></a></li>
							<li><a href="#video/Injustice_Blackest_Night_ENG"><img src="video_jpegs/blackest_night.jpg" alt="image04" /></a></li>
<!-- 							<li><a href="#video/injustice_aquaman_eng_v2"><img src="video_jpegs/aquaman.jpg" alt="image01" /></a></li> -->
							<li><a href="#video/injustice_deathstroke_eng"><img src="video_jpegs/death.jpg" alt="image05" /></a></li>



						</ul>
						<!-- End Elastislide Carousel -->

					</div>
					<h4>WHAT IF OUR GREATEST HEROES BECAME OUR GREATEST THREAT?</h4>
					<p>Injustice: Gods Among Us is an all-new title, development by award-winning NetherRealm Studios. The title debuts a bold new fighting game franchise that introduces a deep, original story featuring a large cast of favourite DC Comics icons such as Batman, Green Arrow, Cyborg, Harley Quinn, Nightwing, Solomon Grundy, Superman, The Flash, Wonder Woman, Green Lantern and many others. Set in a world where the lines between good and evil are blurred, players will experience heroes and villains engaging in epic battles on a massive scale.</p>
				
				<div class="image_carousel" id="character_info">

					<div id="foo">
						<img src="images/characters/batman_sml.png" width="250" height="250" data-name="batman" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/green-arrow_sml.png" width="250" height="250" data-name="green-arrow" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/GreenLantern_sml.png" width="250" height="250"  data-name="green-lantern" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/aquaman_sml.png" width="250" height="250"  data-name="aquaman" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/bane_sml.png" width="250" height="250" data-name="bane-bio" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/black-adam_sml.png" width="250" height="250"  data-name="black-adam" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/catwoman_sml.png" width="250" height="250"  data-name="catwoman" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/cyborg_sml.png" width="250" height="250"  data-name="cyborg" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/Deathstroke_sml.png" width="250" height="250"  data-name="deathstroke" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/Doomsday_sml.png" width="250" height="250"  data-name="dommsday" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/flash_sml.png" width="250" height="250"  data-name="flash-bio" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/grundy_sml.png" width="250" height="250"  data-name="grundy" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/harleyq_sml.png" width="250" height="250"  data-name="harley" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/HawkGirl_sml.png" width="250" height="250"  data-name="hawkgirl" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/joker_sml.png" width="250" height="250"  data-name="joker" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/lex-luthor_sml.png" width="250" height="250"  data-name="lex-luthor" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/nightwing_sml.png" width="250" height="250"  data-name="nightwing" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/Raven_sml.png" width="250" height="250"  data-name="raven" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/shazam_sml.png" width="250" height="250"  data-name="shazam" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/sinestro_sml.png" width="250" height="250"  data-name="sinestro" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/superman_sml.png" width="250" height="250"  data-name="superman" alt="Injustice - Gods Among Us"/>
						<img src="images/characters/wonder-woman_sml.png" width="250" height="250"  data-name="wonder-woman" alt="Injustice - Gods Among Us"/>
					</div>
					<div class="clearfix"></div>
				    <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
				    <a class="next" id="foo2_next" href="#"><span>next</span></a>
				  </div>

				</div>
			</div>

				
			</section>
		<!--end about game-->
	</div>



	</article>

		<footer>
			<div class="top_btn" data-pos="top"><a href="#_"><img src="images/top_btn.jpg"></a></div>
			<div class="row">
				<div class="logos"><img src="images/logos.jpg" alt=""></div>
			</div>


				<p class="small_text row">INJUSTICE: GODS AMONG US software © 2013 Warner Bros. Entertainment Inc. Developed by NetherRealm Studios.  All other trademarks and copyrights are the property of their respective owners.  ”  ”, “PlayStation”, “PS3”, “  " and “   “ are trademarks or registered trademarks of Sony Computer Entertainment Inc. KINECT, Xbox, Xbox 360, Xbox LIVE, and the Xbox logos are trademarks of the Microsoft group of companies and are used under license from Microsoft. Wii U is a trademark of Nintendo. © 2012 Nintendo. All rights reserved.</p>
				<p class="small_text row">DC LOGO, and all characters, their distinctive likenesses, and related elements are trademarks of DC Comics© 2013. </p>
				<p class="small_text row">NETHERREALM STUDIOS LOGO, WB GAMES LOGO, WB SHIELD: ™ &amp; © Warner Bros. Entertainment Inc. </p>

				<!--footer section-->
				<!-- start footer -->    
				<div id="ign_footer">
					<div class="ign-elements">
						<div class="row">
							<section class="six columns">
			               		<p class="corp-terms">Copyright 2013 <span class="white">IGN Entertainment UK, Inc.</span><br>
			               		 <a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy Policy</a> | <a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User Agreement</a></p>
			            	</section>
				            <section class="six columns right">
				                <p class="corp-links"><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a> | <a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a> | <a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></p>
				            </section>
				          </div>
					</div>
			    </div>
				<!--end footer-->
		</footer>

	</div>



	<!--vote count group-->
	<div id="vote_count">
		<span class="group_0 lex_luther"></span>
		<span class="group_0 the_joker"></span>
		<span class="group_1 batman"></span>
		<span class="group_1 superman"></span>
		<span class="group_2 flash"></span>
		<span class="group_2 green_lantern"></span>
		<span class="group_3 wonder_woman"></span>
		<span class="group_3 cat_woman"></span>
		<span class="group_4 bane"></span>
		<span class="group_4 doomsday"></span>
	</div>
	<!-- end vote count group -->

	<!-- Included JS Files (Uncompressed) -->
	<script src="javascripts/foundation/jquery.js"></script>
	
	<script src="javascripts/foundation/jquery.cookie.js"></script>
	
	<script src="javascripts/foundation/jquery.foundation.reveal.js"></script>

	<script src="javascripts/scrollTo.js"></script>

	<script src="javascripts/jquerypp.custom.js"></script>

	<script src="javascripts/jquery.elastislide.js"></script>

	<script type="text/javascript" src="javascripts/jquery.carouFredSel-6.2.0-packed.js"></script>	

	<script src="javascripts/videojs.patched.min.js"></script>

	<script src="javascripts/waypoints.min.js"></script>

	<script src="javascripts/waypoints-sticky.min.js"></script>


  <!-- Application Javascript, safe to override -->
  <script src="javascripts/foundation/app.js"></script>

  <script type="text/javascript">

  (function( jQuery ) {
  if ( window.XDomainRequest ) {
    jQuery.ajaxTransport(function( s ) {
      if ( s.crossDomain && s.async ) {
        if ( s.timeout ) {
          s.xdrTimeout = s.timeout;
          delete s.timeout;
        }
        var xdr;
        return {
          send: function( _, complete ) {
            function callback( status, statusText, responses, responseHeaders ) {
              xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
              xdr = undefined;
              complete( status, statusText, responses, responseHeaders );
            }
            xdr = new window.XDomainRequest();
            xdr.onload = function() {
              callback( 200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType );
            };
            xdr.onerror = function() {
              callback( 404, "Not Found" );
            };
            xdr.onprogress = function() {}; 
            if ( s.xdrTimeout ) {
              xdr.ontimeout = function() {
                callback( 0, "timeout" );
              };
              xdr.timeout = s.xdrTimeout;
            }

            xdr.open( s.type, s.url, true );
            xdr.send( ( s.hasContent && s.data ) || null );
          },
          abort: function() {
            if ( xdr ) {
              xdr.onerror = jQuery.noop();
              xdr.abort();
            }
          }
        };
      }
    });
  }
})( jQuery );

  </script>

	<script type="text/javascript">
		
		$( '#carousel' ).elastislide();

		/**video**/
		  var myplayer1 = _V_("video_1");
		  var myplayer2 = _V_("video_2");
		  var myplayer3 = _V_("video_3");
		  var myplayer4 = _V_("video_4");
		  var myplayer5 = _V_("video_5");
		  var myplayer6 = _V_("video_6");
		  var myplayer7 = _V_("video_7");
		  var myplayer8 = _V_("video_8");
		  var myplayer9 = _V_("video_9");
		  var myplayer = _V_("video_10");
		  var myplayerTrailers = _V_("video_trailers");


	  // var video_1, video_2, video_3, video_4, video_5, video_6, video_7, video_8, video_9, video_10;
   //    function onYouTubeIframeAPIReady() {
   //      video_1 = new YT.Player('player', {
   //        width: '100%',
   //        height: '352'
   //      });

   //     video_2 = new YT.Player('player2', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_3 = new YT.Player('player3', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_4 = new YT.Player('player4', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_5 = new YT.Player('player5', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_6 = new YT.Player('player6', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_7 = new YT.Player('player7', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_8 = new YT.Player('player8', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_9 = new YT.Player('player9', {
   //        width: '100%',
   //        height: '352'
   //      });

   //      video_10 = new YT.Player('player10', {
   //        width: '100%',
   //        height: '352'
   //      });
   //    }



$(window).load(function() {

	// Using default configuration
	$("#foo").carouFredSel({
		//responsive: true,
		width: '100%',
		items: {
			width: 250,
			height: 250
		},
		circular: false,
		infinite: false,
		auto 	: false,
		prev	: {	
			button	: "#foo2_prev",
			key		: "left"
		},
		next	: { 
			button	: "#foo2_next",
			key		: "right"
		},
		duration: "auto"
	});
	
});

		/**end video**/
		
     /* Voting api */
    $(document).ready(function(){
        
      var _ignapi = {
        'url': 'http://54.247.91.128/vote.php?_=' + Math.random()
      };
      
      /* Load votes on launch */
      $.ajax({
        url: _ignapi.url,
        type: 'GET',
        data: 'all',
        cache: 'false'
      }).success(function(msg){
        res = $.parseJSON(msg);
        for(var fighter in res) {
          $('#vote_count > span.' + fighter).text(res[fighter]);
        }
      });
      
      /* Register votes silently */
      $('button.vote_btn').click(function(){
        $('.radio_element.ticked').each(function(){
    
          var ajaxType = "POST";
    
          if ($.browser.msie) {
            ajaxType = "GET";
          }
          
          $.ajax({
            url: _ignapi.url,            
            type: ajaxType,
            cache: false,
            data: { "winner" : $(this).attr('data-fighter') } ,
            error: function(jqXHR, textStatus, errorThrown) {
            }
          }).success(function(msg){
          });
          
        });
      });
    });

    
	</script>
	<div id="modal_content">

		<div class="batman"><h4>BATMAN</h4><p>Millionaire Bruce Wayne was just a kid when he watched his parents get gunned down during a mugging in Gotham City. The crime would define his life, as he dedicated himself to becoming the world’s greatest weapon against crime—the Batman.
<br>
<br>
Forget his Batarangs, Batmobile, or Utility Belt filled with high-tech weapons. Batman is the most feared superhero of all, because he’s pushed himself to the absolute pinnacle of human achievement. He’s a brilliant detective who’s mastered fighting techniques the world’s barely heard of. An Olympic-caliber athlete with a plan for every occasion, Batman’s seemingly always five steps ahead of his foes. But in his crusade against injustice, two questions always loom: How far will he go to protect the innocent, and will he sacrifice his humanity along the way?</p><a class="close-reveal-modal">&#215;</a></div>

		<div class="green-arrow"><h4>GREEN ARROW</h4><p>Spoiled billionaire Oliver Queen came home with an entirely different personality—and a newfound purpose in life. Completely self-absorbed and never caring about anything but himself, Queen was on a leisurely sailing trip when his assistant betrayed him, leaving him for dead on a desolate remote island. Stranded, he survived by, amongst other things, mastering a bow and arrow. When he eventually discovered the primitive locals that shared the island with him, he uncovered a large conglomerate sadistically exploiting and enslaving them. Ollie used his new skills of survival and archery to lead a rebellion— only to realize how similar his opponents were to the man he used to be.
<br>
<br>
 Returning to civilization only meant more opportunities to watch greed and corruption strangle Joe and Jane Average. Outfitting himself with arrows that shoot tear gas, smoke, nets, and more, Green Arrow has a trick shot for every obstacle. A modern-day Robin Hood, he constantly fights for the little guy as a crusading symbol for revolution. And his days on that island, filled with sweat, blood, and desperation, are a testament to what it takes for someone to answer that wake-up call, change their ways, and fly straight.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="bane-bio"><h4>BANE</h4><p>An orphaned boy imprisoned by tyrants and used as a lab rat for high-risk experiments, Bane broke free from his shackles and grew to become one of the most powerful super-villains in Gotham City. His epic battle with Batman in the historic "Knightfall" storyline where he broke Batman's back and his subsequent appearances in comics, film and animation have made him one of the Dark Knight's most popular and enduring adversaries.
<br>
<br>
 Fueled by the uber-steroid serum known as Venom, Bane's strength is matched only by his intelligence. A master planner and strategist, he devoted his life to proving himself as the best. However, his quest for supremacy puts him directly at odds with the Dark Knight, whom Bane sees as the human embodiment of the law and order responsible for his captivity and torture as a child. Intriguingly, despite this hatred, Bane holds a much greater sense of respect for Gotham's guardian than most of his fellow villains, seeing Batman as his only real equal.
<br>
<br>
 But that equality comes at a price. The same serum that gives the villain his greatest strength is also his greatest weakness. Bane has developed an intense drug-like addiction to Venom and if deprived will go into severe withdrawal that could eventually culminate in madness. But if this will ultimately prove to be his downfall, it certainly hasn't yet.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="black-adam"><h4>BLACK ADAM</h4><p>Long ago, the council of wizards—powerful beings that held dominion over all magic in the world—chose the warrior Black Adam as their champion, granting him vast magical powers and abilities. However, that power only further skewed Adam's already maligned moral compass, and rather than use his newfound gifts to help others, he used it to dominate them.
<br>
<br>
 Adam turned against the wizards and after a brutal battle was imprisoned for thousands of years. But now he is free once more, and his insatiable quest to conquer the world, to control all magic, continues. The only thing that stands in his way is Shazam!, the new champion of magic—and Black Adam will do anything in his power to destroy him and assert his position as the true ruler of the world.
<br>
<br>
 Driven by a need to rule, incredibly arrogant and one of the most powerful beings on the planet, Black Adam lives by a simple creed: kneel at his feet or be crushed by his boot.

</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="catwoman"><h4>CAT WOMEN</h4><p>Growing up an orphan in one of Gotham’s worst neighborhoods, Selina turned to a life of crime to not only keep herself alive but her friends as well. The vivacious villainess elevated cat burglary to an art form, stealing whatever may be necessary—and sometimes even more—to survive and protect those closest to her. Utilizing a cat mask and suit to steal from the city’s rich and corrupt, she's a master at hand-to-hand combat and acrobatics, stealthy as a ninja and an expert at breaking and entering.
<br>
<br>
 However, despite her burglarious nature and antagonistic origins, Catwoman walks a fine line between hero and villain. She balances precariously on the precipice of good and evil, of doing the wrong thing for the arguably right reasons. And as such, Selina doesn't look at the world in black and white, but shades of gray. Guided by her own strict—if not slightly skewed—moral compass, Selina sometimes serves as one of the Dark Knight's closest allies. Their flirtatious relationship and complicated feelings for one another constantly push both in directions neither thought possible...or are always comfortable with. Particularly since there always looms the possibility that one day Catwoman will take her crimes too far, and Batman will have no choice but to see her behind bars.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="cyborg"><h4>CYBORG</h4><p>Although a star athlete, Vic Stone only yearned for his father’s approval. But Dad was too focused on his scientific career to notice… until the day Vic became his greatest experiment. After Vic suffered a grave injury, his father saved him by replacing over half his body with cybernetic parts.
<br>
<br>
Now Cyborg is plugged into every computer on Earth, and no firewall—or brick wall—can keep him out. While his cybernetic enhancements provide superhuman strength, speed, and endurance, that same technology destroys his chances to live as a professional athlete. Now flourishing in the digital realm, Vic is desperately alone in the physical world—and still longs for his father’s affection.  Hungry to find purpose again, he fights alongside the World’s Greatest Super Heroes.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="deathstroke"><h4>DEATH STROKE</h4><p>A military man, Slade Wilson was recruited into the special ops unit Team 7, where he participated in highly dangerous classified missions. Willing to do whatever he could to better himself, Slade underwent an experimental procedure that granted him enhanced speed, strength and intelligence, effectively making him the world's greatest soldier. Following the disbandment of Team 7, an embittered and jaded Slade would use his powers as a mercenary-for-hire and quickly gained a name for himself in the criminal world as the man to call when you want to make sure the job will get done.
<br>
<br>
 Beyond his heightened speed, strength, and senses, Deathstroke's greatest asset comes from his incredibly high intelligence, which makes him a master strategist and tactician. He is capable of standing toe-to-toe against some of the DC Universe's most gifted fighters and most powerful heroes. Never content with an easy assignment, Deathstroke continuously takes on tougher and more difficult tasks, constantly pushing himself further and further with an unwavering focus on getting the job done no matter what it takes, no matter how ruthless, deadly or manipulative he must be.
<br>
<br>
 He is, without question, the world’s greatest assassin and one of the most feared people on the planet.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="dommsday"><h4>DOOMSDAY</h4><p>Doomsday was originally created on Krypton as part of a deranged scientist's quest to create the perfect hunter. The creature was subjected to cruel and terrible experiments—killed and cloned thousands upon thousands of times—a process that allowed his body to adapt natural defenses against whatever previously had bested him. Through this sort of rapid evolution, Doomsday became an unstoppable, nearly indestructible creature of violence and death that knew only hate and sought to kill everything in its path.

<br>
<br>
Doomsday possesses an immeasurable level of strength and invulnerability beyond those of even Superman. His disregard for life and endless rage make him an unpredictable and dangerous force, destruction itself let upon the world.
 </p><a class="close-reveal-modal">&#215;</a></div>
		<div class="flash-bio"><h4>FLASH</h4><p>Young Barry Allen’s life stopped the minute his mother was murdered. The true killer never found, its mystery obsessed Barry, driving him to become a forensic scientist. Consumed by his work, he spent his life chained to his desk, solving every case that flew across it. But when a freak lightning bolt hits a nearby shelf in his lab, Barry receives super-speed, becoming the Flash. Now, he’ll race up buildings, across oceans, and around the world to get his man—while getting introduced to a world so much bigger than his old life of microscopes and cold cases.
<br>
<br>
Able to run at near light-speeds, his powers provide the ultimate caffeine kick: He can run up buildings, move so swiftly he phases through objects, create sonic booms with the snap of his fingers—and never need to order delivery. Despite his speed, Barry can become so obsessed with crime-solving he can still lose track of everything else around him, leaving the fastest man alive constantly running a minute behind.
 </p><a class="close-reveal-modal">&#215;</a></div>
		<div class="grundy"><h4>SOLOMON GRUNDY</h4><p>The undead monster known as Solomon Grundy is powerful, immortal and bad news for every hero unfortunate enough to cross his path. Grundy was a corrupt man named Cyrus Gold until his untimely death in Slaughter Swamp. Since then, his entire existence has been an unending cycle of death and rebirth. When Grundy dies, he eventually rises again, emerging from the depths of his original resting place in the swamp to wreak havoc on the living.
<br>
<br>
 However, there’s a twist to this otherwise dependable cycle. When he’s reborn, his personality often changes. Sometimes he returns a mindless monster, destroying everything in his path. Other times, he’s reborn with a high level of intelligence, granting him a deadly combination of brains and brawn. But no matter the changes in his personality, he always has the same superhuman strength, stamina and near indestructibility, and his desires remain evil and corrupt. He is a monster—an invulnerable one that will keep coming back again…and again…</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="harley"><h4>HARLEY QUIN</h4><p>Before her descent into madness, Dr. Harleen Quinzel was a promising psychiatrist assigned to Arkham Asylum, Gotham's home for the criminally insane. But after meeting the Joker, the young doctor became obsessed with the crazed criminal's warped mind. Sensing opportunity, the Joker manipulated her, driving his therapist so mad that he was able to control her. Harleen fell in love with the villain, broke him out of Arkham and devoted her life to making him happy and spreading his bloody brand of mayhem.
<br>
<br>
 Athletic, agile and dangerously unpredictable in a fight, Harley Quinn is a formidable opponent in her own right. She is willing to do anything in order to further the Clown Prince of Crime's goals—no matter the cost to anyone else or even herself. Her energetic, affable and animated personality is enough to make almost anyone drop their guard—a mistake that could easily cost them their life. Deranged and psychotic, Harley Quinn is the poster girl for chaos' terrifying allure.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="hawkgirl"><h4>HAWKGIRL</h4><p>Born on the planet Thanagar, Shayera Hol joined the police force in her adolescent years and grew to become one of the planet’s fiercest warriors and most decorated officers. During an assignment, Shayera chased a criminal to Earth and became fascinated by the planet and its people. After capturing and delivering the fugitive to Thanagar, Shayera decided to return to Earth and use her experience as a force of good to bring the same level of justice to her new home as she did her old as the high-flying hero known as Hawkgirl.
<br>
<br>
Hawkgirl’s abilities stem from the mysterious gravity-defyting Nth metal only native to her home planet of Thanagar. The metal serves as the basis for much of the planet’s technological advancements and makes the Thanagarian weapon system unique throughout the galaxy.
<br>
<br>
 The metal, coupled with a synthetic pair of wings, allows Hawkgirl to fly and also grants her enhanced strength and the ability to heal more quickly from injury. In battle, she wields swords and maces forged from the mysterious alloy, which allow her to disrupt magic and deflect blasts of energy.  However, regardless of her weapons powerful properties, Shayera is an expert warrior and is incredibly deadly with her trusty mace.
<br>
<br>
 Shayera is dedicated to the pursuit of justice and uses all of her skills and abilities to battle those that would harm the innocent. Her penchant for violence honed growing up on her home planet often makes other heroes and law enforcement from Earth uneasy. Yet despite her warlike nature, Hawkgirl is viewed by both friend and foe as a warrior for justice that will protect the planet and its people with her very life.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="joker"><h4>JOKER</h4><p>The Joker is the polar opposite of the Dark Knight. Both were created by a great tragedy, but Batman has since vowed to do whatever it takes to prevent similar incidents, the Joker revels in creating chaos and destroying lives, believing that life’s a big joke and psychotically demonstrating that in a moment, it can all change.
<br>
<br>
 Not much is known about his past, but his acts during the present are what define the Joker as one of the greatest threats to our heroes and the people they've sworn to protect. He's killed a Robin, crippled Batgirl, and tortured and murdered countless people throughout the DC Universe—all just for a laugh.

</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="lex-luthor"><h4>LEX LUTHER</h4><p>A self-made man whose immeasurable intellect is always in conflict with his equally immeasurable ego, Lex Luthor is one of the world's most brilliant minds and most famous villains. From a young age, Lex utilized his almost unparalleled acumen to build himself a financial empire through hard work and dedication—but mainly through the strategic use of intimidation, bribery and murder. Sacrificing other people became simply a means to an end on the path to success, but Luthor always made sure none of his deplorable acts could be traced back to him. Instead, his climb to power and rise in status made him someone to look up to, someone people wanted to emulate—and he relished in the adoration.
<br>
<br>
 Until it all changed the day Superman appeared and gave the world something else to aspire to.
<br>
 Driven by jealousy masked with hate over what he perceives to be his stolen place as humanity's true savior, Luthor constantly attempts to destroy and humiliate Superman at every possible opportunity. He's willing to lie, cheat, steal, manipulate and go to any extreme necessary to accomplish his goals, not caring one bit about who he hurts. He is a man who not only wants to be seen as a god, but who has convinced himself he's the only one this worlds needs.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="nightwing"><h4>NIGHTWING</h4><p>When his circus acrobat family was brutally murdered, Dick Grayson was taken in by billionaire and bat-friendly super hero Bruce Wayne. Bruce shared his secret life as Batman with the young boy and eventually molded him into Robin—the Dark Knight’s partner in Gotham. But as Dick grew older, playing second fiddle to the sternest hero in the Justice League began to wear on him, and eventually he struck out on his own, staying true to what he learned as Robin while coming into his own as a respected member of the super hero pantheon. His code, like his mentor’s, is simple—if you’re breaking the law on his turf, watch out. Anyone who doesn’t can expect a short and most assuredly painful trip to Arkham.
<br>
<br>
 Yet, while he may share methods and motivation with Batman, Dick knows what being Batman can do. He doesn’t want to become as dark, controlling and lonely as Bruce. But for all the good he’s accomplished while avoiding it, one question is unavoidable—without going as far as Batman, can Nightwing be as effective as he ultimately wants to be?</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="raven"><h4>RAVEN</h4><p>Raised in the alternate dimension of Azarath, the half-human, half-demon Raven grew up having to learn how to control her emotions in order to suppress her demonic powers. But while Raven must avoid experiencing any sort of extreme feeling, one of her demonic abilities allows her to feel and control the emotions of others—letting her experience their sorrow, happiness, joy, loss and more.
<br><br>
Raven is extremely powerfully, gifted with magical, psychic and telekinetic powers as well as the ability to manipulate energy, emotion and shadows. However, despite her vast abilities, the one thing Raven wants the most is the one thing she cannot have: to be normal. She is forever cursed to never truly be herself because of the constant threat of destruction that comes with it.
<br><br>
Despite her need to maintain emotional control, Raven has formed a tight bond with the Teen Titans, considering them her only real family. She is willing do anything to protect them, even if it means surrendering to her own darkness and sacrificing her life in the process.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="shazam"><h4>SHAZAM</h4><p>Unlike most foster children, Billy Batson couldn’t care less about finding a family. He just wants to turn eighteen and finally become an adult. While the boy’s compassionate and kind, he’s gotten used to protecting himself by staying emotionally distant from everybody. But that changes when he meets an ancient wizard who introduces him to one very powerful word. 
<br>
<br>
Just by saying SHAZAM!, Billy is transformed into a magical flying adult superhero, one with the genius of Solomon, the strength of Hercules, the unbreakable will of Atlas, the lightning blasts of Zeus, the power of Achilles and the speed of Mercury. But gaining the power of the gods means inheriting their enemies too. With the abilities of the world’s mightiest mortal, Billy will have to struggle with magical threats as well as his own youthful naïveté. To truly tap into his power, this orphan who’s always kept people at arm’s length will have to learn what family really is.
 </p><a class="close-reveal-modal">&#215;</a></div>
		<div class="sinestro"><h4>SINESTRO</h4><p>Originally hailing from the planet Korugar, Sinestro once served as the Green Lantern of Sector 1417 and quickly gained recognition among the Corps for his ability to police his sector. However, peace came at a price. Sinestro's determination to preserve order caused him to act more as a dictator than a protector, conquering his own home planet in the process. Control became his means to an end, the true path to order in both his sector and the universe as a whole, and Sinestro did anything necessary to achieve it. It wasn't until then new recruit Hal Jordan learned of Sinestro's methods that the Guardians were informed and Sinestro was ejected from the Corps.
<br>
<br>
 Steadfast in his belief of moral superiority and methods of maintaining order, Sinestro formed the Sinestro Corps, harnessing the yellow energy of fear, to not only combat the Guardians, whom he sees as the main obstacle on the path to intergalactic peace, but to also strengthen the Green Lantern Corps. For it is Sinestro’s opinion that the Green Lanterns have long since been held back by their creators.
<br>
<br>
 Highly intelligent with an almost unbreakable will, Thaal Sinestro will do anything to achieve control and thereby order—control over his emotions, control over others, and even control over the entire universe.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="wonder-woman"><h4>WONDER WOMAN</h4><p>Wonder Woman is Princess Diana of the immortal Amazons from Greek mythology.  When army pilot Steve Trevor crashes on the warriors’ secluded island paradise, Diana wins the right to escort him home and make her people known to the world. Entering our cynical world for the first time, there’s a lot she must wrap her head around, especially our ways of war, hate, and, well… dating. Helping her are her superhuman strength and speed, as well as the trademark bulletproof bracelets, but it’s probably her Golden Lasso of Truth most people really wish they had.
<br>
<br>
Torn between a mission to promote peace and her own warrior upbringing, Wonder Woman fights evil while hoping to unlock the potential of a humanity she doesn’t always understand.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="green-lantern"><h4>GREEN LANTERN</h4><p>When just a boy, Hal witnessed his greatest nightmare—his pilot father dying in a tragic plane crash. Nevertheless determined to follow in his footsteps, Hal repressed his fears, becoming a reckless, defiant test pilot. But when a dying alien crashes on Earth, the irresponsible Hal is chosen to be that alien’s successor in the Green Lantern Corps, a universe-wide peacekeeping force over 3,600 members strong.
<br>
<br>
And his life only gets wilder from there. Alien romances. Intergalactic wars. Power-hungry super-villains. Wielding a Green Lantern power ring—a weapon fueled by willpower—he can fly and create constructs made of pure energy, generating anything from massive green fists to emerald rifles that can snipe from a planet away. But while Hal tends to ignore his fears, he’ll learn the only way to truly master his ring’s power is by confronting and overcoming them.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="superman"><h4>SUPERMAN</h4><p>Superman has super-everything—strength, speed, flight, invulnerability as well as his renowned X-ray and heat vision. The most powerful being on the planet, his amazing abilities are also a melancholy reminder of how different he is from the people he’s dedicated to protect.  
<br>
<br>
A universal icon, Superman means different things to the many diverse people he inspires: He’s an alien; an immigrant from a faraway land just looking to help; a country boy fighting the never-ending battle for truth and justice. And recent comics have truly spotlighted his role as the people’s hero: Following a neophyte Man of Steel still learning his powers’ limits, Superman fights the evil corporate tycoons and corrupt one-percenters that have overwhelmed the establishment.</p><a class="close-reveal-modal">&#215;</a></div>
		<div class="aquaman"><h4>AQUAMAN</h4><p>Cast out from Atlantis as a baby, Arthur Curry grew up on land, thinking himself a normal human. But when he matured, Atlantis returned, claiming him as their rightful king. Caught between a surface world constantly ravaging the sea and Atlanteans looking to lash out in revolt, he’s committed to protecting the entire globe. 
<br>
<br>
Aquaman is the king of all things aqua. He can breathe underwater, swim at tremendous speeds, and telepathically communicate with sea life. Being able to withstand ocean depths, he gets bonus points on land with his superhuman strength, enhanced senses, and nearly impenetrable skin. And although his courage and decisive nature have proven him a true heir to Atlantis’s throne, the continual conflict between land and sea makes him a citizen of both—and at home in neither.</p><a class="close-reveal-modal">&#215;</a></div>

	</div>

	<div id="myModal" class="reveal-modal small">
	  
	</div>
	<div id="myModal2" class="reveal-modal">
	  
	</div>


  <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>

  	
	  <!--begin GA script-->
  <script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-15279170-1']);
	  _gaq.push(['_trackPageview', 'Injustice']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	  
	</script>
	<!--end GA script-->

  
  <div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=147333065402611";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	
	<script type="text/javascript">
	  window.___gcfg = {lang: 'en-GB'};
	
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>

  <!-- Begin comScore Tag -->
	<script>
	  var _comscore = _comscore || [];
	  _comscore.push({ c1: "2", c2: "3000068" });
	  (function() {
	    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	    el.parentNode.insertBefore(s, el);
	  })();
	</script>
	<noscript>
	  <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
	</noscript>
   <!-- End comScore Tag -->
   <!--
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
-->


</body>
</html>
