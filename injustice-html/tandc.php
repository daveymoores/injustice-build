<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if lte IE 9]> <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Injustice</title>

	<!-- Included CSS Files -->
    <link rel="stylesheet" type="text/css" href="stylesheets/demo.css" />
	<link rel="stylesheet" type="text/css" href="stylesheets/elastislide.css" />
	<link rel="stylesheet" type="text/css" href="stylesheets/custom.css" />

	<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
	<link rel="stylesheet" href="stylesheets/app.css">


	<script src="javascripts/foundation/modernizr.foundation.js"></script>
	<script src="javascripts/modernizr.custom.17475.js"></script>

	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->

</head>
<body class="age_gate tandc">

	<header id="ignHeaderHeader">
		<div id="ignHeader" class="clear">

				
				<div id="ignHeader-userBar">
					<div class="container"> <a id="ignHeader-logo" href="./"></a>
					</div>
				</div>

		</div>
	</header>

	<div class="standard_wrapper">

		<img src="images/backgrounds/hero_title.png" alt="Injustice - Gods Among Us" class="title">
		
	<section id="character_stats" class="standard_container">
		<div class="row">
			<div class="container">


	<h4>GAME OF CHANCE PROMOTION<h4>
	<h4>TERMS &amp; CONDITIONS</h4>
	   
<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>1.<span
style="mso-spacerun:yes">&nbsp; </span>Information on how to enter forms part
of the terms and conditions of entry to this game of chance promotion. Entry
into the promotion is deemed acceptance of these terms and conditions. <br>
&nbsp;<br>
2.<span style="mso-spacerun:yes">&nbsp; </span></span><span lang=EN-AU
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman"'>The Promoter is IGN Entertainment
UK Ltd, 55 New Oxford Street, London WC1 1ABS. (ÒThe PromoterÓ) <o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-AU style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>The Sponsor is Warner Bros. Entertainment UK Limited, 98 Theobalds
Road, London WC1X 8WB. (ÒThe SponsorÓ)<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>&nbsp;<br>
3.<span style="mso-spacerun:yes">&nbsp; </span>The promotion commences at 10 am
(BST) on 10 April 2013 and closes at 11am (BST) on 22 April 2013 (ÒThe
Promotion PeriodÓ). <br>
&nbsp;<br>
4.<span style="mso-spacerun:yes">&nbsp; </span>Entry is open to users who are
resident in the United Kingdom and 18 (eighteen) years of age or over.</span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'> (ÒEligible
EntrantsÓ)</span><span lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US'><br>
<br>
5.<span style="mso-spacerun:yes">&nbsp; </span>Directors, management,
employees, officers and immediate families of the Promoter and Sponsor and their
related bodies corporate, employees of associated agencies involved in this
promotion are ineligible to enter this promotion. <br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>6.</span><span lang=EN-AU
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman"'> To enter the Competition, entrants
must fill in their details on the competition entry page located at: </span><span
lang=EN-AU><a href="http://uk-microsites.ign.com/injustice"><span
style='font-size:12.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial'>http://uk-microsites.ign.com/injustice</span></a></span><span
lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><br>
</span><span lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;
line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>One
entry per person for the duration of the Promotion Period.<br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></span><span lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>7.<span
style="mso-spacerun:yes">&nbsp; </span>The winning entries will be randomly
drawn from all Eligible EntrantsÕ entries at the offices of Harkable, 18
Highbury New Park, London, N5 2DB, on</span><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> </span><span lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>22
April 2013 at 1pm.</span><span lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US'>&nbsp;The first eligible entry drawn wins the Main
Prize. The subsequent nine (9) eligible entries drawn shall win a Runner-Up
Prize. There will be a total of ten (10) winners.<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><span
style="mso-spacerun:yes">&nbsp;</span><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>8.<span
style="mso-spacerun:yes">&nbsp; </span>The outcome of the draw is final and no
correspondence will be entered into. <o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>9. The Main Prize is:</span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0cm' type=disc>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) Samsung UE46ES6300 3D TV model. The estimated retail value
     is £859.94 (eight hundred and fifty nine pounds and ninety four pence
     sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) Samsung HT E4500 surround sound system. The estimated
     retail value is £199.97 (one hundred and ninety nine pounds and ninety
     seven pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>The choice of one (1) 4GB Xbox 360 games console or one (1) 12GB
     PlayStation 3 Super Slim games console or one (1) 8GB Nintendo Wii U Basic
     Pack games console. </span><span lang=EN-AU style='font-size:11.0pt;
     mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
     "Times New Roman"'>The estimated retail value of 1 (one) </span><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>4GB Xbox 360 games console is £142.21 (one hundred and forty two
     pounds and twenty one pence sterling). The estimated retail value of one
     (1) 12GB PlayStation 3 Super Slim games console is £139 (one hundred and
     thirty nine pounds sterling). The estimated retail value of one (1) 8GB
     Nintendo Wii U Basic Pack games console is £199 (one hundred and ninety
     nine pounds sterling)<o:p></o:p></span></li>
</ul>

<p class=MsoListParagraph style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
6.0pt;margin-left:36.0pt;text-indent:-18.0pt;line-height:150%;mso-list:l1 level1 lfo2'><![if !supportLists]><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol'><span
style='mso-list:Ignore'>á<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-AU style='font-size:11.0pt;
mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>One (1) additional controller for the console of the
winnerÕs choice. The estimated retail value of 1 (one) Xbox 360 controller is
£25 (twenty five pounds sterling).The estimated retail value of 1 (one)
PlayStation 3 controller is £32.74 (thirty two pounds and seventy four pence
sterling). The estimated retail value of 1 (one) Wii U Pro Controller is £32.99
(thirty two pounds and ninety nine pence sterling)<o:p></o:p></span></p>

<ul style='margin-top:0cm' type=disc>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>The choice of one (1) collectorÕs edition copy of Injustice: Gods
     Among Us on either Xbox 360 or PlayStation 3 or one (1) standard edition
     copy of Injustice: Gods Among Us on WiiU. The estimated retail value of
     one (1) collectorÕs edition copy of Injustice: Gods Among Us on Xbox 360
     or PlayStation 3 is £65 (sixty five pounds sterling). The estimated retail
     value of one (1) standard edition copy of Injustice: Gods Among Us on WiiU
     is £37.99 (thirty seven pounds and ninety nine pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) 49-litre drinks mini fridge. The estimated retail value is
     £109.99 (one hundred and nine pounds and ninety nine pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>Fifty (50) cans of soft drink up to a maximum value of £20 (twenty
     pounds sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) copy of the Dark Knight Trilogy on Blu-ray. The estimated
     retail value is £33 (thirty three pounds sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) copy of the Superman 5 Film Collection on Blu-ray. The
     estimated retail value is £29.97 (twenty nine pounds and ninety seven
     pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) copy of the Green Lantern on Blu-ray. The estimated retail
     value is £11.85 (eleven pounds and eighty five pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) Injustice Batman bobblehead figurine and one (1) Injustice
     Superman bobblehead figurine. The estimated retail value of each is £17.99
     (seventeen pounds and ninety nine pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) copy of the book 75 Years of DC Comics: That Art of Modern
     Mythmaking. The estimated retail value is £86.40 (eighty six pounds and
     forty pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) copy of the book DC Comics Ultimate Character Guide. The
     estimated retail value is £6.89 (six pounds and eighty nine pence
     sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>£100 (one hundred pounds sterling) worth of pizza vouchers<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) three (3) metre HDMI cable. The estimated retail value is
     £4.45 (four pounds and forty five pence sterling)<o:p></o:p></span></li>
 <li class=MsoNormal style='line-height:150%;mso-list:l1 level1 lfo2'><span
     lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:
     150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
     EN-US'>One (1) two (2) metre optical digital audio cable. The estimated
     retail value is £5.44 (five pounds and forty four pence sterling)<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>The Runner-Up Prize is:<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='text-indent:-18.0pt;line-height:150%;
mso-list:l3 level1 lfo5'><![if !supportLists]><span lang=EN-US
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-ansi-language:
EN-US'><span style='mso-list:Ignore'>á<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-US style='font-size:11.0pt;
mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>One (1) standard edition copy of
Injustice: God Among Us on a platform of the winnerÕs choice (limited to Xbox
360, PlayStation 3 or Wii U). The estimated retail value is £37.99 (thirty
seven pounds and ninety nine pence sterling)<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-AU style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><br>
10. The prizes must be taken by the prize winners as stated and cannot be
transferred to another person or exchanged for other goods and services.<span
style="mso-spacerun:yes">&nbsp; </span>There is no cash alternative to any of
the prizes in whole or in part. Any element of the prize which is unused will
be forfeited and no compensation will be paid in lieu of that element of the prize.
The prizes are awarded on an &quot;as is&quot; basis and neither Promoter,
Sponsor nor any of their related bodies corporate, make any representations or
warranties of any nature with respect to the prizes.<br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-AU style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>11. Except for any liability that cannot be excluded by law,
the Promoter and the Sponsor (including their officers, employees and agents)
excludes all liability (including negligence), for any personal injury; or any
loss or damage (including loss of opportunity); whether direct, indirect,
special or consequential, arising in any way out of the Promotion, including,
but not limited to, where arising out of the following: any theft, unauthorised
access or third party interference; or any tax liability incurred by the
winner.<br style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-AU style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>12. The Promoter and the Sponsor accept no responsibility
for any variation of any aspect of the prize, due to circumstances outside its
control. In any such event, an alternative element of the prize will be
arranged.<o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><br>
13.<span style="mso-spacerun:yes">&nbsp; </span>The Prize Winner will be
notified by e-mail on 23 April 2013. </span><span lang=EN-AU style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>If the Prize Winner does not reply to the winnerÕs email
acknowledging receipt of their prize, by </span><span lang=EN-US
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:EN-US'>30 April
2013</span><span lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;
line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>, they
will forfeit their prize. A list of prize winners can be made available on
request by writing to the offices of the Promoter: IGN Entertainment UK Ltd, 55
New Oxford Street, London, WC1A 1BS. A stamped addressed envelope must be
included with the request.</span><span lang=EN-US style='font-size:11.0pt;
mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>14.<span
style="mso-spacerun:yes">&nbsp; </span>The Promoter </span><span lang=EN-AU
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman"'>and the Sponsor are</span><span
lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-US'> not responsible for any problem or technical malfunction of any
communications network or any late, lost, incorrectly submitted, delayed,
ineligible, incomplete, corrupted or misdirected entry whether due to error,
transmission interruption or otherwise. </span><span lang=EN-AU
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman"'>The Promoter and the Sponsor reserve
the right to disqualify any entrant submitting an entry which, in the opinion
of the Promoter and/ or the Sponsor, includes objectionable content, including
but not limited to profanity, nudity, potentially insulting, scandalous,
inflammatory or defamatory images or language. The promoterÕs decision is final
and no correspondence will be entered into. </span><span lang=EN-AU
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:EN-US'><span
style="mso-spacerun:yes">&nbsp;</span></span><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><br>
&nbsp;<br>
15. </span><span lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;
line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>If
for any reason this competition is not capable of running as planned, including
infection by computer virus, bugs, tampering, unauthorised intervention, fraud,
technical failures or any other causes beyond the control of the Promoter and
the Sponsor which corrupt or affect the administration security, fairness,
integrity or proper conduct of this competition, the Promoter and the Sponsor reserve
the right in its sole discretion to disqualify any individual who tampers with
the entry process, and to cancel, terminate, modify or suspend the competition.
The Promoter and the Sponsor assume no responsibility for any error, omission,
interruption, deletion, defect, delay in operation or transmission,
communications line failure, theft or destruction or unauthorised access to, or
alteration of, entries. The Promoter and the Sponsor are not responsible for
any problems or technical malfunction of any telephone network or lines,
computer online systems, servers or providers, computer equipment, software,
failure of any email or entry to be received by the Promoter and/ or the
Sponsor on account of technical problems or traffic congestion on the Internet
or at any Website, or any combination thereof, including any injury or damage
to participant's or any other person's computer related to or resulting from
participation or downloading any materials in this competition. CAUTION: ANY
ATTEMPT TO DELIBERATELY DAMAGE ANY WEBSITE OR THE INFORMATION ON A WEBSITE, OR
TO OTHERWISE UNDERMINE THE LEGITIMATE OPERATION OF THIS COMPETITION, MAY BE A
VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE,
WHETHER SUCCESSFUL OR NOT, THE PROMOTER AND THE SPONSOR RESERVE THE RIGHT TO
SEEK DAMAGES TO THE FULLEST EXTENT PERMITTED BY LAW.</span><span lang=EN-US
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:EN-US'><br>
&nbsp;<br>
16.<span style="mso-spacerun:yes">&nbsp; </span>Entry details become the
property of the Promoter. Eligible Entrants consent to the Promoter </span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>and the Sponsor </span><span
lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-US'>using their names, country of residence and entry details </span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>for promotional
and/or marketing purposes. The prize winners agree to participate in reasonable
publicity activities surrounding the promotion as requested by the Promoter
and/ or the Sponsor.<br style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></span><span lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:"Times New Roman";
mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>17. Without limiting the foregoing,
Eligible EntrantsÕ personal information provided in connection with this
promotion will be handled in accordance </span><span style='font-size:11.0pt;
line-height:150%;font-family:Arial;mso-ansi-language:EN-GB'>with data
protection legislation and in accordance</span><span style='color:#1F497D;
mso-ansi-language:EN-GB'> </span><span lang=EN-US style='font-size:11.0pt;
mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'>with the PromoterÕs privacy policy (see
</span><span lang=EN-AU><a href="http://corp.ign.com/privacy.html"><span
lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-US'>http://corp.ign.com/privacy.html</span></a></span><span lang=EN-US
style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:
Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:EN-US'>) </span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>and the SponsorÕs
privacy policy (see</span><span lang=EN-AU> <a
href="http://www.warnerbros.co.uk/#/privacy"><span lang=EN-US style='font-size:
11.0pt;line-height:150%;font-family:Calibri;mso-bidi-font-family:Calibri;
mso-ansi-language:EN-GB'><span lang=EN-US>http://www.warnerbros.co.uk/#/privacy</span></span></a></span><span
lang=EN-AU style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>).</span><span
lang=EN-US style='font-size:11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;
font-family:Arial;mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:150%'><span lang=EN-US style='font-size:
11.0pt;mso-bidi-font-size:10.0pt;line-height:150%;font-family:Arial;mso-bidi-font-family:
"Times New Roman";mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>


			</div>
		</div>
	</section>
	</div>

</body>
</html>