<?php
header('Access-Control-Allow-Origin: *');

class Character {
  private $_id;
  private $_opponent;
  private $_votes = 0;
  private $_db;
    
  function __construct($id, $opponent) {
    $this->_db = new PDO('mysql:host=localhost;dbname=ign_injustice;charset=utf8;', 'ign_injustice', 'ign_injustice');
    $this->_id = $id;
    $this->_opponent = $opponent;
    $stmt = $this->_db->prepare("SELECT votes FROM characters WHERE id=:char_id");
    $stmt->bindValue(':char_id', $this->_id);
    $stmt->execute();
    $this->_votes = $stmt->fetchColumn();
  }
  
  function record_vote() {
    $this->_votes = $this->_votes + 1;
    $stmt = $this->_db->prepare("UPDATE characters SET votes=:votes WHERE id=:id");
    $stmt->bindValue(':votes', $this->_votes);
    $stmt->bindValue(':id', $this->_id);
    $stmt->execute();
  }
  
  function get_votes() {
    return $this->_votes;
  }
  
  function get_opponent() {
    return $this->_opponent;
  }
  
}

$characters = array();
$characters['lex_luther'] = new Character(0, 'the_joker');
$characters["the_joker"] = new Character(1, 'lex_luther');   
$characters["batman"] = new Character(2, 'superman');   
$characters["superman"] = new Character(3, 'batman');   
$characters["flash"] = new Character(4, 'green_lantern');      
$characters["green_lantern"] = new Character(5, 'flash');
$characters["wonder_woman"] = new Character(6, 'cat_woman');
$characters["cat_woman"] = new Character(7, 'wonder_woman');  
$characters["bane"] = new Character(8, 'doomsday');
$characters["doomsday"] = new Character(9, 'bane');

if (isset($_POST['winner']) && isset($characters[$_POST['winner']])) {
  $characters[$_POST['winner']]->record_vote();
  $votes      = $characters[$_POST['winner']]->get_votes();
  $opp_votes  = $characters[$characters[$_POST['winner']]->get_opponent()]->get_votes();
  $pct_agree  = ($votes / ($votes + $opp_votes)) * 100.0; 
  $results = array();
  $results[$_POST['winner']] = $pct_agree;
  $results[$characters[$_POST['winner']]->get_opponent()] = 100.0 - $pct_agree;
  echo json_encode($results);
} else if (isset($_GET['all'])) {
  $results = array();
  foreach($characters as $k => $c) {
    $results[$k] = $c->get_votes();
  }
  echo json_encode($results);
} else if (isset($_GET['analytics'])){
  $count = 0;
  foreach($characters as $k => $c) {
    print $k . ': ' . $c->get_votes() . "</br<";
    $count += $c->get_votes();
  }
  print "==========</br>";
  print "Total: " . $count;
}
else {
  echo 'What? Don\'t get it.';
}

?>